#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"

//https://gitlab.scss.tcd.ie/aronesa/supplemental_microprocessor: Project Repository

// Must declare the main assembly entry point before use.
void main_asm();
/**
* @brief Supplemental Assignment...
*
* @return int Returns exit-status zero on completion.
*/

/**
 * @brief Wrapper to allow the assembly code to call the gpio_init()
 *        SDK function.
 * 
 * @param pin       The GPIO pin number to initialise.
 */
void asm_gpio_init(int pin) {
    gpio_init(pin);
}

/**
 * @brief Wrapper to allow the assembly code to call the gpio_set_dir()
 *        SDK function.
 * 
 * @param pin       The GPIO pin number of which to set the direction.
 * @param dir       Specify the direction that the pin should be set to (0=input/1=output).
 */
void asm_gpio_set_dir(int pin, int dir) {
    gpio_set_dir(pin, dir);
}


/**
 * @brief Wrapper to allow the assembly code to call the gpio_get()
 *        SDK function.
 * 
 * @param pin       The GPIO pin number to read from.
 * @return int      Returns the current value of the GPIO pin.
 */
int asm_gpio_get(int pin) {
    return gpio_get(pin);
}

/**
 * @brief Wrapper to allow the assembly code to call the gpio_put()
 *        SDK function.
 * 
 * @param pin       The GPIO pin number to write to.
 * @param value     Specify the value that the pin should be set to (0/1).
 */
void asm_gpio_put(int pin, int value) {
    gpio_put(pin, value);
}


/*----------------------------ADC Wrapper Functions-------------------------*/
//Documentation: https://www.raspberrypi.com/documentation/pico-sdk/hardware.html#group_hardware_adc

/**
 * @brief Wrapper to allow the assembly code to call the adc_init()
 *        SDK function.
 */
void asm_adc_init() {
    adc_init();
}

/**
 * @brief Wrapper to allow the assembly code to call asm_adc_gpio_init to 
*           select the input channel for the ADC.
 * 
 * @param pin The GPIO pin number to select as the ADC input.
 */
void asm_adc_gpio_init(uint pin) {
    adc_gpio_init(pin);
}

/**
 * @brief Wrapper to allow the assembly code to call asm_adc_select_input 
 *            to select the ADC input.
 * 
 * @param The ADC input channel (0-3 for GPIO26-GPIO29).
 */
void asm_adc_select_input(uint input) {
    adc_select_input(input);
}

/**
 * @brief Wrapper to allow the assembly code to call asm_adc_read 
 *            to read the ADC value.
 * 
 * @return uint16_t Returns the current value of the ADC.
 */
uint16_t asm_adc_read() {
    return adc_read();
}

int main() {

 // Jump into the main assembly code subroutine that implements the project.
 main_asm();
 // Returning zero indicates everything went okay.
 return 0;
}
